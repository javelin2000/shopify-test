<?php

namespace App\Http\Controllers;

use App\Repositories\ReservationRepository;
use App\Repositories\WorkshopRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ShopifyController extends Controller
{
    /**
     * @var WorkshopRepository
     */
    protected $repository;

    /**
     * view template for index method
     * @var string
     */
    protected $indexTemplate = 'shopify/index';

    /**
     * header Content-Type value
     * @var string
     */
    protected $contentType = 'application/liquid';

    protected $reservationRepository;

    /***
     * ShopifyController constructor.
     * @param WorkshopRepository $workshopRepository
     * @param ReservationRepository $reservationRepository
     */
    public function __construct(WorkshopRepository $workshopRepository, ReservationRepository $reservationRepository)
    {
        $this->repository = $workshopRepository;
        $this->reservationRepository = $reservationRepository;
    }

    /**
     * @return JsonResponse
     */
    public function removeApp()
    {
        logger("Remove APP request " . request()->method(), ['data' =>request()->all(), 'header' => request()->header()] );
        return response()->json(['status'=>'Ok'], 200);
    }

    /**
     * @return JsonResponse
     */
    public function getCustomer()
    {
        logger("getCustomer request " . request()->method(), ['data' =>request()->all(), 'header' => request()->header()]  );
        return response()->json(['status'=>'Ok'], 200);
    }

    /**
     * @return JsonResponse
     */
    public function eraseCustomer()
    {
        logger("eraseCustomer  request " . request()->method(), ['data' =>request()->all(), 'header' => request()->header()]  );
        return response()->json(['status'=>'Ok'], 200);
    }

    /**
     * @return Response
     */
    public function index()
    {
        try {
            $data = [
                'timeTableByDay' => $this->repository->getTimeByDay(),
                'leaders' => $this->reservationRepository->getLeaders()

            ];
            $response = response()->view($this->indexTemplate, $data, 200);

            return $response;
        } catch (\Exception $exception) {
            logger($exception->getMessage(), [
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
            ]);
            return redirect()->route('nome');
        }

    }
}
