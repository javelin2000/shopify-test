<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Reservation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "customer_name",
        "phone",
        "email",
        "is_leader",
        "reservation_id",
        "workshop_id",
    ];


    /**
     * @Relation
     * @return BelongsTo
     */
    public function workshop() {
        return $this->belongsTo(Workshop::class);
    }
    /**
     * @Relation
     * @return BelongsTo
     */
    public function reservation() {
        return $this->belongsTo(Reservation::class);
    }

    /**
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Reservation::class, 'reservation_id');
    }

    /**
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany(Reservation::class, 'reservation_id');
    }

}
