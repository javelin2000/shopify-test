<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\Repositories\WorkshopRepository;
use Illuminate\Http\JsonResponse;


class BookingController extends Controller
{
    protected $workshopRepository;
    protected $reservationRepository;

    public function __construct(WorkshopRepository $workshopRepository)
    {
        $this->workshopRepository = $workshopRepository;
    }

    /**
     * @param BookingRequest $request
     * @return JsonResponse
     */
    public function booking(BookingRequest $request)
    {
        try {
            $validatedData = $request->all();
            if ($this->workshopRepository->createReservation($validatedData)) {
                return view('shopify.complete', ['message' => 'Successfully complete !!!']);
            }
            throw new \Exception();
        } catch (\Exception $exception) {
            return view('shopify.complete', ['message' => 'Something is wrong. But we are working on it.']);

        }
    }
}
