<?php


namespace App\Repositories;


use App\Models\Workshop;
use Illuminate\Database\Eloquent\Collection;

class WorkshopRepository extends BaseRepository
{
    /**
     * WorkshopRepository constructor.
     * @param Workshop $workshop
     */
    public function __construct(Workshop $workshop)
    {
        parent::__construct();
        $this->model = $workshop;
    }

    /**
     * @param array $filters
     * @return mixed
     */
    public function getTimeByDay(array $filters = []) {
        $timeTable = $this->model::orderBy('date')->get();

        $timeByDay = $timeTable->mapToGroups(function ($item, $key) {
            return [
                $item->date->format('Y-m-d') => [
                    'id' => $item->id,
                    'time' => sprintf('%s - %s', $item->from->format('H:i'), $item->to->format('H:i')),
                    'info' => $item->max_customers - $this->getWorkshopReservations($item)->count(),
                ]
            ];
        });

        return $timeByDay;
    }

    /**
     * @param Workshop $workshop
     * @return Collection
     */
    public function getWorkshopReservations(Workshop $workshop): Collection
    {
        return $workshop->reservations;
    }

    /**
     * Create new reservation
     * @param $data
     * @return array
     */
    public function createReservation($data)
    {
        $workshop = $this->model->findOrFail($data['workshop_id']);
        $reservation = $workshop->reservations()->create($data);
        if (isset($data['guest']) && count($data['guest']) ){
            $guests =  (collect($data['guest']))->map( function ($value) use($workshop){
                return array_merge($value, ['workshop_id' => $workshop->id]);
            });
            $reservation->children()->createMany($guests);
        }

        return true;
    }
}