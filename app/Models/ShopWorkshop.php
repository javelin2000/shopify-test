<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ShopWorkshop extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['shop_id','workshop_id'];

    /**
     * @Relation
     * @return BelongsTo
     */
    public function shop() {
        return $this->belongsTo(Shop::class);
    }

    /**
     * @Relation
     * @return BelongsTo
     */
    public function workshop() {
        return $this->belongsTo(Workshop::class);
    }

    /**
     * @Relation
     * @return HasMany
     */
    public function customers() {
        return $this->hasMany(Reservation::class);
    }
}
