<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'required|string|max:255',
            'phone' => 'required|string|min:9|max:100',
            'is_leader' => 'filled|boolean',
            'workshop_id' => 'required|numeric',
            'guest.*customer_name' => 'string|max:255',
            'guest.*.email' => 'email|string|min:5|max:100',
        ];
    }
}
