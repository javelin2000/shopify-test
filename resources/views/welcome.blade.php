@extends('layouts.shopify')

@section('content')
    <p>Yoy are: {{ ShopifyApp::shop() ?? 'no shop data' }}</p>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            window.location.href = "https://javelin-first-test-app.myshopify.com";
        })
    </script>

    <script type="text/javascript">
        var AppBridge = window['app-bridge'];
        var actions = AppBridge.actions;
        var TitleBar = actions.TitleBar;
        var Button = actions.Button;
        var Redirect = actions.Redirect;
        var titleBarOptions = {
            title: 'Welcome',
        };
        var myTitleBar = TitleBar.create(app, titleBarOptions);
    </script>
@endsection