<?php


namespace App\Repositories;


use App\Models\Reservation;

class ReservationRepository extends BaseRepository
{
    /**
     * WorkshopRepository constructor.
     * @param Reservation $reservation
     */
    public function __construct(Reservation $reservation)
    {
        parent::__construct();
        $this->model = $reservation;
    }

    /**
     * create new reservation
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->model->create($data);
    }


    public function getLeaders()
    {
        return $this->model->where('is_leader', true)->with('children')->with('workshop')->get();
    }
}