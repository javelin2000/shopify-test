@extends('shopify-app::layouts.default')


@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@stop

@section('content')
    <div class="container" >
        <div class="section-header text-center">
            <h3>{{$message}}</h3>
        </div>

    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            setTimeout(()=>{
                window.location.href = "/index";
            }, 5000)
        })

    </script>
@stop
