var guestCount = 0;

document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById("bookingForm");
    const addGuestBtn = document.getElementById("addGuestBtn");
    const timeSelect = document.getElementById("timeChoiceSelect");

    /**
     * form submit listener
     */
    form.addEventListener("submit", function (event) {
        event.preventDefault();
        let timeSelector = document.getElementById('timeChoiceSelect');
        console.log('timeSelector.dataset.info', timeSelector.dataset.info);
        console.log('guestCount', guestCount);
        console.log('parseInt(timeSelector.dataset.info) > guestCount', parseInt(timeSelector.dataset.info) > guestCount);

        if (parseInt(timeSelector.dataset.info) > guestCount) {
            const result = form.submit();

        } else {
            alert('Sorry, there are only ' + timeSelect.dataset.info + ' more available spots for this workshop');
        }
        return false;
    });

    /**
     * add new guest button listener
     */
    addGuestBtn.addEventListener("click", function (event) {
        event.preventDefault();
        addGuest()
    });

    function toFirstUpperCase(word) {
        return word.replace(/^\w/, c => c.toUpperCase());
    }

    /**
     * create label for input Node
     */
    function createLabel(type, count) {
        let guestLabel = document.createElement('label');
        guestLabel.setAttribute("for", "guest" + toFirstUpperCase(type) + "Input_" + count);
        guestLabel.innerHTML = "Guest " + type;
        return guestLabel;
    }

    /**
     * create input Node
     */
    function createInput(type, count) {
        let guestInput = document.createElement('input');
        guestInput.classList.add('form-control');
        guestInput.id = 'guest' + toFirstUpperCase(type) + 'Input_' + count;
        guestInput.type = type === 'email' ? type : 'text';
        guestInput.name = 'guest[' + count + ']' + '[' + type === 'email' ? 'email' : 'customer_name' + ']';
        guestInput.name = `guest[${count}][${type === 'email' ? 'email' : 'customer_name'}]`;
        return guestInput;
    }

    /**
     * @param count
     * @return {HTMLDivElement}
     */
    function createGuestNameNode(count) {
        const guestName = document.createElement('div');
        guestName.classList.add('form-group');
        const guestNameLabel = createLabel('name', count);
        const guestNameInput = createInput('name', count);

        guestName.appendChild(guestNameLabel);
        guestName.appendChild(guestNameInput);
        return guestName;
    }

    /**
     * @param count
     * @return {HTMLDivElement}
     */
    function createGuestEmailNode(count) {
        const guestEmail = document.createElement('div');
        guestEmail.classList.add('form-group');

        const guestEmailLabel = createLabel('email', count);
        const guestEmailInput = createInput('email', count);
        guestEmail.appendChild(guestEmailLabel);
        guestEmail.appendChild(guestEmailInput);
        return guestEmail;
    }


    /**
     * add new guest fields
     */
    function addGuest() {
        if (guestCount + 1 < parseInt(timeSelect.dataset.info)) {
            guestCount++;
            const guestName = createGuestNameNode(guestCount);
            const guestEmail = createGuestEmailNode(guestCount);

            let guestContainer = document.getElementById('guestContainer');
            guestContainer.appendChild(guestName);
            guestContainer.appendChild(guestEmail);
        } else {
            alert('Sorry, there are only ' + timeSelect.dataset.info + ' more available spots for this workshop');
        }

    }
});

/**
 * update Data Attribute
 * @param e
 */
function updateDataInfo(e) {
    e.dataset.info = e.options[e.selectedIndex].dataset.info;
    document.getElementById("addGuestBtn").disabled = false;
}

function addTimeSelect(e) {
    let value = JSON.parse(e.options[e.selectedIndex].value);
    console.log(value);
    let time = document.getElementById('timeChoiceSelect');
    time.disabled = false;
    time.innerHTML = '<option disabled selected>Make you choice...</option>';
    for (let key in value) {
        let elem = document.createElement("option");
        if (value[key].info > 0) {
            elem.innerText = value[key].time + " empty slots: " + value[key].info;
            elem.value = value[key].id;
            elem.dataset.info = value[key].info;
            time.appendChild(elem);
        }
    }
}
