<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use OhMyBrew\ShopifyApp\Models\Shop as OhMyBrewShop;

class Shop extends OhMyBrewShop
{
    /**
     * @Relation
     * @return HasMany
     */
    public function reservations() {
        return $this->hasMany(Reservation::class);
    }
}
