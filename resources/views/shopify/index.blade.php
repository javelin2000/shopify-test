@extends('shopify-app::layouts.default')


@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@stop

@section('content')
    <div class="container">
        <div class="section-header text-center">
            <h3>Please, fill out the form </h3>
        </div>

        <div id="reservation-wrapper" class="form-vertical">
            <div class="container" style="border: 1px solid darkgray; padding: 25px">
                <form id="bookingForm" action="/booking" method="POST">
                    <div class="form-group">
                        <label for="customerNameInput">Customer Name</label>
                        <input type="text" class="form-control" id="customerNameInput" name="customer_name" required>
                        <small id="customerNameHelp" class="form-text text-muted">Enter your name hare.</small>
                    </div>
                    <div class="form-group">
                        <label for="phoneInput">Phone</label>
                        <input type="text" class="form-control" id="phoneInput" name="phone" required>
                    </div>
                    <input type="text" hidden name="is_leader" value="1">

                    <div class="form-group">
                        <label for="dayChoiceSelect">Select day</label>
                        <select class="form-control" id="dayChoiceSelect" onchange="addTimeSelect(this)" required>
                            <option disabled selected>Make you choice...</option>
                            @foreach($timeTableByDay as $day=>$value)
                                <option value="{{$value}}">{{$day}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="timeChoiceSelect">Select time</label>
                        <select class="form-control" id="timeChoiceSelect" disabled name="workshop_id" data-info=""
                                onchange="updateDataInfo(this)" required>
                            <option disabled selected>Make you choice...</option>
                        </select>
                    </div>
                    <hr>
                    <div id="guestContainer">

                    </div>
                    <button class="btn btn-sm btn-primary" id="addGuestBtn" disabled>Add guest</button>


                    <hr>
                    @csrf

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <hr>
    <div class="container" style="padding: 25px 0 ">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Leader Name</th>
                <th scope="col">Phone</th>
                <th scope="col">Workshop</th>
                <th scope="col">Guests list</th>
            </tr>
            </thead>
            <tbody>
            @foreach($leaders as $leader)
                <tr>
                    <th scope="row">{{$leader->id}}</th>
                    <td>{{$leader->customer_name}}</td>
                    <td>{{$leader->phone}}</td>
                    <td>{{$leader->workshop->date->format('d-m-Y')}}
                        <br>
                        {{$leader->workshop->from->format('H:i') }}-{{$leader->workshop->to->format('H:i') }}
                    </td>
                    <td>
                        @if ($leader->children)
                            @foreach($leader->children as $children)
                                <p>{{$children->customer_name}} | {{$children->email}}</p>
                            @endforeach
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('scripts')
    <script src="/js/index.js"></script>
@stop
