<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopWorkshopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_workshop', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('shop_id')->nullable();
            $table->unsignedBigInteger('workshop_id')->nullable();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('SET NULL');
            $table->foreign('workshop_id')->references('id')->on('workshops')->onDelete('SET NULL');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_workshop');
    }
}
