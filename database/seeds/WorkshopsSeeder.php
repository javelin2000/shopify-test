<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class WorkshopsSeeder extends Seeder
{
    protected $data = [
        // June 1st
        [
            'day' => 1,
            'month' => 6,
            'from' => 9,
            'to' => 12,
            'max_customers' => 5
        ],
        [
            'day' => 1,
            'month' => 6,
            'from' => 12,
            'to' => 15,
            'max_customers' => 5
        ],
        [
            'day' => 1,
            'month' => 6,
            'from' => 15,
            'to' => 18,
            'max_customers' => 7
        ],
        // June 2nd
        [
            'day' => 2,
            'month' => 6,
            'from' => 9,
            'to' => 12,
            'max_customers' => 5
        ],
        [
            'day' => 2,
            'month' => 6,
            'from' => 12,
            'to' => 15,
            'max_customers' => 5
        ],
        [
            'day' => 2,
            'month' => 6,
            'from' => 15,
            'to' => 18,
            'max_customers' => 12
        ],
        // June 3nd
        [
            'day' => 3,
            'month' => 6,
            'from' => 9,
            'to' => 12,
            'max_customers' => 5
        ],
        [
            'day' => 3,
            'month' => 6,
            'from' => 12,
            'to' => 15,
            'max_customers' => 7
        ],
        [
            'day' => 3,
            'month' => 6,
            'from' => 15,
            'to' => 18,
            'max_customers' => 5
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            \App\Models\Workshop::create([
                'date' => Carbon::createFromDate(null, $item['month'], $item['day']),
                'from' => Carbon::createFromTime($item['from']),
                'to' => Carbon::createFromTime($item['to']),
                'max_customers' => $item['max_customers']
                ]);
        }
    }
}
