<?php

namespace App\Models;

use App\Repositories\ReservationRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{

    protected $fillable = [
        'date',
        'from',
        'to',
        'max_customers'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date',
    ];

    /**
     * General time mutator.
     *
     * @return Carbon
     */
    public function getDateTime($value) {
        return (clone $this->date)->setTimeFromTimeString($value);
    }


    /**
     * Get From Time
     * @param $value
     * @return Carbon
     */
    public function getFromAttribute($value) {
        return $this->getDateTime($value);
    }

    /**
     * Get To Time
     * @param $value
     * @return Carbon
     */
    public function getToAttribute($value) {
        return $this->getDateTime($value);
    }

    /**
     * @Relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservations() {
        return $this->hasMany(Reservation::class);
    }
}
